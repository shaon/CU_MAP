package cumap;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class CUFRAME {

    public static void main(String[] args) {
        
        SplashScreen start = new SplashScreen();
        
        start.setVisible(true);
        
        try {
            
            Thread.sleep(5000);                         //wait in millisec
            start.dispose();
        
        } catch (InterruptedException ex) {
            System.out.println(ex.getMessage());
        }
        
        JFrame ob = new JFrame("CU MAP");
        
        ob.add(new MainPnl());
        
        //ob.setSize(1200, 650);
        
        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();   //Screen er majhkhane anar jonno
        
        ob.setBounds((screenSize.width-1200)/2, (screenSize.height-640)/2, 1200, 640);
        
        //This is to change the GUI into nimbus
        try { 
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel"); 
            //UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel"); 
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) { 
            JOptionPane.showMessageDialog(null, ex.getMessage()); 
        }
        
        ob.setVisible(true);
        
        ob.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
    }
}
