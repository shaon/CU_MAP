package cumap;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

class ImageZoom {

    Imagepnl imagePanel;
    JSpinner spinner;
    public ImageZoom(Imagepnl ip) {
        imagePanel = ip;
    }

    public JPanel getUIPanel(){
        SpinnerNumberModel model = new SpinnerNumberModel(1.0, 0.1, 1.4, .01);
        spinner = new JSpinner(model);
        spinner.setPreferredSize(new Dimension(45, spinner.getPreferredSize().height));

        spinner.addChangeListener(new ChangeListener() {

            public void stateChanged(ChangeEvent e) {
                float scale = ((Double) spinner.getValue()).floatValue();
                imagePanel.setScale(scale);
            }
        });
        JPanel panel = new JPanel();
        panel.add(new JLabel("Zoom"));
        panel.add(spinner);
        return panel;
    }
}
