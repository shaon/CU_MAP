package cumap;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

class Imagepnl extends JPanel implements MouseListener
{
	
	BufferedImage image;
    double scale;
    
    
    double initialx[] = {758, 681, 372, 322, 755, 280, 584, 448, 400, 106, 754, 721, 238, 553, 931, 764, 357, 711, 322, 62, 475, 484, 692, 687, 526, 775, 214, 392, 433, 808, 801, 732, 722, 746, 775, 731, 822, 815, 790, 767, 703, 629, 659, 601, 655, 677, 648, 650, 539, 430, 419, 404, 434, 410, 252, 227, 227, 106, 429, 637, 830, 812, 764, 817, 842};
    double initialy[] = {292, 125, 224, 281, 113, 281, 329, 293, 276, 359, 65, 161, 306, 93, 529, 307, 294, 50, 291, 330, 376, 443, 224, 231, 329, 194, 328, 293, 238, 350, 291, 227, 201, 181, 156, 126, 134, 113, 64, 15, 13, 20, 42, 88, 81, 51, 152, 200, 201, 270, 275, 258, 293, 311, 293, 292, 280, 331, 329, 428, 375, 406, 432, 481, 456};
    int len=0;
    
    Integer nod[];                  //<--** look closely 'Integer' type
     
    double[] placesx = new double[100];
    double[] placesy = new double[100];

    double xs, ys, xd, yd;
    int fstSel, secSel, itemNo = 0, itNo = 0;
    InfoPnl obj;
    
    FloydWrs shortest;
    
    public Imagepnl(InfoPnl obImPnl) 
    {
        loadImage();                            //methode for loading the Image
        
        scale = 1.0;
        setBackground(Color.black);
        
        obj=obImPnl;
        
       
        for(int i=0; i<initialx.length; i++)
        {
            placesx[i]=initialx[i];
            placesy[i]=initialy[i];
            
            itemNo++;
        }
  
        itNo = itemNo;                  //itNo is used for 'customization'
        
        shortest = new FloydWrs();       //V.V.I--- This is where the FloydWarshall Algo runs only once... :)
        //System.out.println("To Test that the Algo Runs Only Once... :D");
        
        addMouseListener(this);
        
    }

    public void pathRefresh()
    {
        len =0;
        
        scale = 1.0;
        this.setScale(scale);
        
        obj.distance = 0;
        repaint();
    }
    
    public void coOrdinate()                    //Selected node gulor position 'places' array te rakhbe 
    {
        
        fstSel = obj.cbxs.getSelectedIndex();
        secSel = obj.cbxd.getSelectedIndex();
        
        if(fstSel == secSel)
            obj.distance = 0;
        else
            obj.distance = (int)shortest.cost[fstSel][secSel];
        
        
        ArrayList<Integer> path = shortest.printPath(fstSel, secSel);
        
        nod = new Integer[path.size()];                   //ArraryList er proti jader khutkhuni ase tader jonno array diye kora holo :P
        nod = path.toArray(nod);
        len=nod.length;
        
        path.clear();
        
/*
        System.out.println("len = "+path.size());                 
        for(int j=0; j<path.size() ; j++)
            System.out.print(" "+path.get(j));                   //path.get(j) is mode line path[j]
*/
        
        repaint();
    }
    
    @Override
    public void paintComponent(Graphics gr) 
    {
        super.paintComponent(gr);
        Graphics2D g2d = (Graphics2D) gr;                                //Graphics type 'gr' k type cast korlam Grapics2D te
        //g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                //RenderingHints.VALUE_INTERPOLATION_BICUBIC);
        int w = getWidth();
        int h = getHeight();
        int imageWidth = image.getWidth();
        int imageHeight = image.getHeight();
        double x = (w - scale * imageWidth) / 2;                                //y & y hocche image er (0,0) postion ta kototuku shift hocche seta
        double y = (h - scale * imageHeight) / 2;
        AffineTransform at = AffineTransform.getTranslateInstance(x, y);        //Co-ordinate Transform korlam...after Zoom...
        at.scale(scale, scale);
        
        g2d.drawRenderedImage(image, at);                            //Image ta k draw korlam....
        
       
        
        for (int i = itemNo; i < itNo; i++)                                    //This part is for Customization....
        {
            gr.drawRect((int)placesx[i], (int)placesy[i], 16, 15);
            gr.setColor(Color.BLUE);                                         //font color set kortesi
            gr.setFont(new Font("courier", Font.BOLD, 14));
            gr.drawString(i + "", (int)placesx[i]+1 , (int)placesy[i] + 12);

            
            gr.setColor(Color.BLACK);
        }
        

        g2d.setStroke(new BasicStroke());                     //Strock : line er width thik korbe

        float[] path_col = new float[3];
        Color.RGBtoHSB(29, 239, 34, path_col);             //RGB value nilam(paint :P) and RGB to HSB convert korlam
        
        g2d.setPaint(Color.getHSBColor(path_col[0], path_col[1], path_col[2]));           //***or setColor()
        
        float dashes[] = {15};                                   //dash er Length
        g2d.setStroke(new BasicStroke(5, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_BEVEL, 10, dashes, 0));
        
        double xx = at.getScaleX();                            //xx & yy hocche koto gun boro or choto holo image ta
        double yy = at.getScaleY();
        
        for(int i=0; i<len - 1; i++)
        {
            xs = placesx[nod[i]];
            ys = placesy[nod[i]];
            xd = placesx[nod[i+1]];
            yd = placesy[nod[i+1]];
            
            g2d.draw(new Line2D.Double(xs*xx+x, ys*yy+y, xd*xx+x, yd*yy+y));            //Eikhanei jotto kahini korsi... Zoom In-Out er jonno...
        
        }
        
        g2d.setStroke(new BasicStroke(3));                         //Sudhu line er width
        
        g2d.setPaint(Color.RED);
        
        for(int i=0; i<len ; i++)                                   //node gulo k circle kore LAL batti diye indicate korlam...
        {
            g2d.draw(new Ellipse2D.Double( (placesx[nod[i]]-5)*xx+x, (placesy[nod[i]]-5)*yy+y, 10*xx, 10*yy)); 
        }
      
    }

    //For the scroll pane.
    
    @Override
    public Dimension getPreferredSize() {
        int w = (int) (scale * image.getWidth());
        int h = (int) (scale * image.getHeight());
        return new Dimension(w, h);
    }

    public void setScale(double s) {
        scale = s;
        revalidate(); // update the scroll pane
        repaint();
    }

    private void loadImage() {
        String fileName = "map.jpg";
        
        try {
            URL url = getClass().getResource(fileName);
            //System.out.println(url);
            image = ImageIO.read(url);
            
        } catch (MalformedURLException mue) {
            JOptionPane.showMessageDialog(null,"URL trouble: " + mue.getMessage());
        } catch (IOException ioe) {
            JOptionPane.showMessageDialog(null,"read trouble: " + ioe.getMessage());
        }
    }

   
	@Override
    public void mouseClicked(MouseEvent e) 
    {
        
        if (obj.customize.isSelected())                         //This is also for customization feature... 
        {   
            int x, y;
            
            x = e.getX();
            y = e.getY();

            //System.out.println(x+" , "+y);

            obj.cbxs.addItem(itNo);
            obj.cbxd.addItem(itNo);


            placesx[itNo] = x;
            placesy[itNo] = y;

            itNo++;
            
        }
        repaint();
        
    }
    @Override
    public void mousePressed(MouseEvent e) {  }

    @Override
    public void mouseReleased(MouseEvent e) {  }

    @Override
    public void mouseEntered(MouseEvent e) {  }

    @Override
    public void mouseExited(MouseEvent e) {  }

}