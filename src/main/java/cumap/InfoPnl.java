package cumap;

import java.awt.Color;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;


class InfoPnl extends JPanel
{
    JComboBox cbxs, cbxd;
    JButton btn,refrash;
    JLabel lbl,srtstPath;
    JCheckBox customize;
    
    int distance;              
    
    String node[]={"1 No.Gate", "A.F.Rahman Hall", "Abdur Rab Hall", "Administrative", "Alaul Hall", "Arts Faculty", "B.K.Jiya Hall", "BBA Faculty", "Bank", "Bio.Faculty", "CU School & College", "Central Mosque", "Chakshu", "Eng'r. Office", "Forestry Faculty", "Guest House", "IT Faculty", "Law Faculty", "Library", "Marine Science", "Pritilata Hall", "Research Center", "S.Amanat Hall", "S.Jalal Hall", "S.Nahar Hall", "S.Wardi Hall", "Science Faculty", "Shahid Minar", "Social Sci.Faculty", "S.Campus Mosque", "Station"};
    
    
    public InfoPnl()
    {
        setLayout(new FlowLayout(FlowLayout.LEFT, 10, 10));
        
        cbxs = new JComboBox();
        cbxd = new JComboBox();
        
        //To add initial nodes in Combobox
        for(int i=0; i<node.length ; i++)
        {
            cbxs.addItem(node[i]);
            cbxd.addItem(node[i]);
        }
        
        btn = new JButton("Draw Path");
        btn.setBackground(Color.PINK);
        
        refrash = new JButton("Refresh");
        
        customize = new JCheckBox("Customize", false);
        
        lbl = new JLabel("Source");
        lbl.setLabelFor(cbxs);
        add(lbl);
        add(cbxs);
        
        add(new JLabel("Destination"));
        add(cbxd);
        add(btn);
        add(refrash);
        add(customize);
        
        srtstPath = new JLabel("Shortest Path = ");
        add(srtstPath);
        
        setBackground(Color.WHITE);
        
    }
}
