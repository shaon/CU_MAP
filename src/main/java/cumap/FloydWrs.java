package cumap;

import java.util.ArrayList;
import java.util.Arrays;

public class FloydWrs {

    final int node_len = 65;                                //   number of nodes
    final double maxi = Double.MAX_VALUE;
                                   
    double cost[][] = new double[node_len][node_len];
    int path[][] = new int[node_len][node_len];
    
    ArrayList<Integer> nodes = new ArrayList<Integer>();

    void initialize() {
        
        for (int i = 0; i < node_len; i++) {
                Arrays.fill(cost[i], maxi);                 //2D array k at a time fill kore dilam
            }
    }

    void warshall() {
        
        int i, j, k;
        
        for (i = 0; i < node_len; i++) {
            for (j = 0; j < node_len; j++) {
                if (cost[i][j] != maxi) {
                    path[i][j] = i;
                }
            }
        }
        
        
        double temp;
        for (k = 0; k < node_len; k++) {
            for (i = 0; i < node_len; i++) {
                for (j = 0; j < node_len; j++) 
                {
                    temp =cost[i][k] + cost[k][j];
                    
                    if (/*cost[i][k] !=maxi && cost[k][j] !=maxi &&*/ cost[i][j] > temp) 
                    {
                        cost[i][j] = temp;
                        path[i][j] = path[k][j];
                    }
                }
            }
        }
    
    }

    
    ArrayList<Integer> printPath(int n1, int n2) 
    {
        if (n1 == n2) {
            nodes.add(n1);
        } 
        else 
        {
            printPath(n1, path[n1][n2]);

            nodes.add(n2);
        }
        return nodes;
    }
    

    public FloydWrs() {
        
        initialize();

        // followings are cost of edges
        
        cost[0][7] = 310.00;
        cost[0][15] = 16.15;
        cost[0][30] = 43.01;
        cost[0][31] = 70.00;

        cost[1][35] = 50.00;
        cost[1][46] = 42.63;

        cost[2][51] = 46.69;

        cost[3][5] = 42.00;
        cost[3][18] = 10.00;
        cost[3][51] = 85.16;

        cost[4][35] = 27.29;
        cost[4][37] = 60.00;

        cost[5][56] = 53.00;

        cost[6][24] = 58.00;

        cost[7][49] = 29.20;
        cost[7][52] = 14.00;

        cost[8][27] = 18.78;
        cost[8][50] = 19.02;

        cost[9][57] = 28.0;

        cost[10][38] = 36.01;

        cost[11][33] = 32.01;

        cost[12][26] = 32.55;
        cost[12][54] = 19.10;

        cost[13][43] = 48.25;

        cost[14][63] = 123.69;

        cost[15][29] = 61.52;

        cost[16][18] = 35.12;
        cost[16][27] = 35.01;

        cost[17][45] = 34.01;

        cost[18][54] = 70.02;

        cost[19][57] = 44.01;

        cost[20][21] = 67.60;
        cost[20][58] = 65.76;
        cost[20][59] = 170.14;

        cost[22][31] = 40.11;

        cost[23][31] = 45.17;

        cost[24][58] = 97.00;

        cost[25][32] = 53.46;

        cost[26][57] = 108.04;

        cost[27][50] = 32.44;
        cost[27][52] = 42.00;
        cost[27][53] = 25.45;

        cost[28][49] = 32.14;

        cost[29][59] = 187.94;
        cost[29][60] = 33.30;

        cost[31][32] = 27.85;

        cost[32][33] = 31.24;
        cost[32][47] = 72.00;

        cost[33][34] = 38.28;

        cost[34][35] = 53.25;
        cost[34][36] = 51.89;

        cost[36][37] = 22.13;

        cost[37][38] = 55.00;

        cost[38][39] = 54.12;

        cost[39][40] = 64.03;

        cost[40][41] = 74.33;
        cost[40][45] = 46.04;

        cost[41][42] = 37.20;

        cost[42][43] = 74.02;

        cost[43][44] = 54.45;
        cost[43][46] = 79.40;

        cost[44][45] = 37.20;

        cost[46][47] = 48.04;

        cost[47][48] = 111.00;

        cost[48][49] = 129.00;

        cost[49][50] = 12.08;

        cost[50][51] = 22.67;
        cost[50][52] = 23.43;

        cost[52][53] = 30.00;

        cost[53][58] = 26.17;

        cost[54][55] = 25.01;

        cost[55][56] = 12.00;

        cost[60][64] = 81.88;

        cost[61][62] = 54.58;

        cost[62][63] = 72.18;

        cost[63][64] = 35.35;      ////Total 76 edges...

        
        // below I m just assigning same value for both directions

        
        for (int i = 0; i < node_len; i++) {
            for (int j = 0; j < node_len; j++) {
                if (cost[i][j] != maxi) {
                    cost[j][i] = cost[i][j];
                }
            }
        }


        warshall();

        //System.out.printf("cost is %d\n", cost[1][5]);

        //printPath(1, 5);    // printing path

        //System.out.printf("\n");
    }
}
