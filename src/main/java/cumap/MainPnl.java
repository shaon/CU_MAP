package cumap;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.JScrollPane;


class MainPnl extends JPanel implements ActionListener
{
    InfoPnl pnl1;
    Imagepnl pnl2;
    ImageZoom zoom;
    
    public MainPnl()
    {
        setLayout(null);                  
        
        pnl1 = new InfoPnl();
        
        pnl1.setBounds(20, 200, 170, 300);
        add(pnl1);
        
        pnl2 = new Imagepnl(pnl1);
        
        zoom = new ImageZoom(pnl2);
        
        JPanel ZoomPnl;
        ZoomPnl = zoom.getUIPanel();
        ZoomPnl.setBounds(20, 150, 170, 30);
        add(ZoomPnl);
        
        
        JScrollPane obJsc = new JScrollPane(pnl2);
        obJsc.setBounds(200, 20, 960, 580);
        add(obJsc);
        
        setBackground(Color.LIGHT_GRAY);
       
        pnl1.btn.addActionListener(this);
        pnl1.refrash.addActionListener(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) 
    {
        if(e.getActionCommand().equals("Refresh"))          //Button Refrash click korle...
        {
            pnl2.pathRefresh();
            pnl1.srtstPath.setText("Shortest Path = ");
            pnl1.srtstPath.setForeground(Color.BLACK);
            zoom.spinner.setValue(1.0);
        }
        else                                                //Button ta jodi Draw Path hoy
        {
            pnl2.coOrdinate();  
            pnl1.srtstPath.setText("Shortest Path = "+pnl1.distance);          //Sortest path dekhanor jonno (int)
            pnl1.srtstPath.setForeground(Color.RED);
            revalidate();
        }
    }
}
